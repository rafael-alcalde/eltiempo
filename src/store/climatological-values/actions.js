import api from '../Api'
import * as type from './types'

export default {
  getClimatologicalValues ({ commit }, { idema, initDate, endDate }) {
    commit(type.REQUEST_CLIMATOLOGICAL_VALUES)
    api.get(`valores/climatologicos/diarios/datos/fechaini/${initDate}/fechafin/${endDate}/estacion/${idema}/`)
      .then(res => {
        api.get(res.data.datos).then(res => {
          commit(type.REQUEST_CLIMATOLOGICAL_VALUES_SUCCESS, {
            payload: res.data
          })
        })
      })
      .catch(error => {
        commit(type.REQUEST_CLIMATOLOGICAL_VALUES_ERROR, { error })
      })
  }
}
