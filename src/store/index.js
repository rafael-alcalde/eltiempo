import Vue from 'vue'
import Vuex from 'vuex'

import stations from './stations'
import station from './station'
import climatologicalValues from './climatological-values'

Vue.use(Vuex)

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Store instance.
 */

// export default function (/* { ssrContext } */) {
const Store = new Vuex.Store({
  modules: {
    stations,
    station,
    climatologicalValues
  },

  // enable strict mode (adds overhead!)
  // for dev mode only
  strict: process.env.DEV
})

// return Store
export default Store
// }
