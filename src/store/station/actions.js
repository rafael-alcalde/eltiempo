import api from '../Api'
import * as type from './types'

export default {
  getStation ({ commit, rootGetters }, { id }) {
    const cachedStation = rootGetters['stations/station'](id)
    if (typeof cachedStation !== 'undefined') {
      commit(type.CACHE_HIT_STATION, {
        payload: cachedStation
      })
    } else {
      commit(type.REQUEST_STATION)
      api.get(`valores/climatologicos/inventarioestaciones/estaciones/${id}`)
        .then(res => {
          api.get(res.data.datos).then(res => {
            commit(type.REQUEST_STATION_SUCCESS, {
              payload: res.data[0]
            })
          })
        })
        .catch(error => {
          commit(type.REQUEST_STATION_ERROR, { error })
        })
    }
  }
}
