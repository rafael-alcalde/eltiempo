import api from '../Api'
import * as type from './types'

export default {
  getStations ({ commit, state }) {
    if (state.data.length > 0) {
      commit(type.CACHE_HIT_STATIONS)
    } else {
      commit(type.REQUEST_STATIONS)
      api.get('valores/climatologicos/inventarioestaciones/todasestaciones')
        .then(res => {
          api.get(res.data.datos).then(res => {
            commit(type.REQUEST_STATIONS_SUCCESS, {
              payload: res.data
            })
          })
        })
        .catch(error => {
          commit(type.REQUEST_STATIONS_ERROR, { error })
        })
    }
  }
}
